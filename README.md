# Travel Smart

Your IRCTC travel guide that finds trains for you, within your budget.

### App not running ?

- The API in use is provided by a third-party with limited request counts. In case the threshold is reached, there will be inappropriate response and the application might break down.


### How to Use ?

- Clone the repository.
- Open the index.html file in a code editor ( ex. VS Code ) .
- Run a live server ( usually Ctrl+Shift+L, in VSCode ) .