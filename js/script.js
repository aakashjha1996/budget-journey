const d = new Date();   // for date popup
const destination = $('#destination'); //destination station input
const source = $('#source'); // source station input
const stations = [
    source,
    destination
]; //creating array of stations
const sourceAutocomplete = $('#source-autocomplete'); // source station autocomplete 
const destinationAutocomplete = $('#destination-autocomplete'); // destination station autocomplete
const autoComplete = [
    sourceAutocomplete,
    destinationAutocomplete
]; //creating array of source and destination autocomplete
const date = $("#date"); // date input
const priceMax = $("#budget"); // budget input
const trainClass = $('#class'); // train class input
const button = $("#submit"); //submit button input
const back = $('#back'); //back button input

let sourceStationId = ''; // storing source station id fetched from server
let destinationStationId = ''; // storing source station id fetched from server
let result = false; 
let totalTrains = 0, currentTrains = 0, flag=false;

$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    format: 'dd-mm-yyyy',
    formatSubmit: 'yyyy/mm/dd',
    select: 'dd/mm/yyyy',
    closeOnSelect: false, // Close upon selecting a date,
    disable: [
        { from: [0000,0,0], to: [d.getFullYear(),d.getMonth(),d.getDate()-1] }
      ]
}); // formatting data popup

$(document).ready(() => {
    if(navigator.onLine) {
        $('#result-card').css({
            'display': 'none'
        });
        $('.parallax').parallax();
        $('select').material_select();
    } else {
        console.log('No Connection');
    }
}); //preparing parallax effect, select option and result card on page load

function validateForm() {
    console.log( $("#budget").val() + ' ' + trainClass.val());
    if ( source.val() !== '' && destination.val !== '' && date.val() !== '' && priceMax.val() !== '' && trainClass !== '')
        return true;
    return false;
} // to check if all fields are valid

function formToggle(toggle) {
    destination.attr("disabled",toggle);
    source.attr("disabled",toggle);
    date.attr("disabled",toggle);
    priceMax.attr("disabled",toggle);
    trainClass.attr("disabled",toggle);
} // to enable and disable form

function loaderToggle(toggle) {
    if (toggle === true) {
        $("#loader").css({
            'display': 'inline-block'
        });
        $("#loader").addClass("active");
    } else {
        $("#loader").css({
            'display': 'none'
        });
        $("#loader").removeClass("active");
    }
} // to toggle loading spinner

source.on("keyup", () => {
    try{
        if ( source.val().length > 3 ) {
            clearAutocomplete(0);
            fetch('https://api.railwayapi.com/v2/suggest-station/name/'+ source.val() +'/apikey/e9hyw3mvg3/').then(resp => {
                return resp.json();
            }).then( function (data) {
                populate(data,0);
            });
        } else {
            clearAutocomplete(0);
        }
    } catch(e) {
        console.log('Server Error');
    }
}); // to catch key strokes on source station input field and make request to server to fetch autocomplete list

destination.on("keyup", () => {
    if ( destination.val().length > 3 ) {
        clearAutocomplete(1);
        fetch('https://api.railwayapi.com/v2/suggest-station/name/'+ destination.val() +'/apikey/e9hyw3mvg3/').then(resp => {
            return resp.json();
        }).then( function (data) {
            populate(data, 1);
        });
    } else {
        clearAutocomplete(1);
    }
}); // to catch key strokes on destination station input field and make request to server to fetch autocomplete list

function populate(data, val) {
    autoComplete[val].empty();
    autoComplete[val].addClass("collection");
    autoComplete[val].addClass("autocomplete-content");
    autoComplete[val].append(generateAutoFillList(data.stations, val));
    autoComplete[val].css("position","absolute");
    autoComplete[val].css("zIndex","10");
    autoComplete[val].css("cursor","pointer");
} // to populate autocomplete list with data fetched from server

function generateAutoFillList(stations, val) {
    try{
        let list = '', stnCode;
        for (let i = 0; i< stations.length ; i++) {
            list += "<a class='collection-item'";
            stnCode =  stations[i].code ;
            if ( val === 0 ) {
                list += "onclick='selectSourceAutocomplete(\""+ stnCode +"\")'>"+ stations[i].name +"</a>";
            } else {
                list += "onclick='selectDestinationAutocomplete(\""+ stnCode +"\")'>"+ stations[i].name +"</a>";
            }
        }
        return list;
    } catch(e) {
        console.log('Server error');
    }
} // to make items in the autocomplete list to be clicked

function clearAutocomplete(val) {
    autoComplete[val].removeClass("collection");
    autoComplete[val].removeClass("autocomplete-content");
    autoComplete[val].empty();
} // to clear autocomplete list

function selectSourceAutocomplete(stnCode) {
    sourceStationId = stnCode;
    source.val(event.target.innerText);
    clearAutocomplete(0);
} // to populate source station input with the cliked autocomplete item

function selectDestinationAutocomplete(stnCode) {
    destinationStationId = stnCode;
    destination.val(event.target.innerText);
    clearAutocomplete(1);
} // to populate destination station input with the cliked autocomplete item

function getTrainsBetweenStations() {
    fetch('https://api.railwayapi.com/v2/between/source/'+ sourceStationId +'/dest/'+ destinationStationId +'/date/'+ date.val() +'/apikey/e9hyw3mvg3/').then(resp => {
        return resp.json();
    }).then(data => {
        console.log(data);
        totalTrains = data.trains.length;
        checkEachTrain(data);
    });
} // fetch trains between source and destination stations from server

function checkEachTrain(data) {
    if ( 0 !== data.trains.length ) {
        for( let i = 0; i < data.trains.length; i++ ) {
            getSeatAvailability(data.trains[i].number);
        }
    } else {
        console.log('Sorry, No Trains');
        noResult();
    }
} // check availability of each train that runs between the two stations

function getSeatAvailability(trainNo) {
    fetch('https://api.railwayapi.com/v2/check-seat/train/'+ trainNo +'/source/'+ sourceStationId +'/dest/'+ destinationStationId +'/date/'+ date.val() +'/pref/'+ trainClass.val() +'/quota/GN/apikey/e9hyw3mvg3/').then(resp => {
        console.log(resp);
        return resp.json();
    }).then(data => {
        if ( data.response_code !== 404 ) {
            checkAvailability(data, trainNo);
            currentTrains++;
            console.log(false);
            flag = true;
        } else {
            currentTrains++;
            console.log("Sorry, no seats for this date of train : " + trainNo + " and "+ currentTrains);
            if ( currentTrains >= totalTrains && flag ) {
                noResult();
                console.log(true);
            }
        }
    });
} // get seat availability for each train

function checkAvailability(data, trainNo) {
    console.log('Data : ' + data);
    currentDate = data.availability;
    for (let i = 0; i < currentDate.length; i++ ) {
        console.log(currentDate[i]);
        if ( i === 0 ) {
            checkFare(trainNo,currentDate[i].status);
        }
    }
} // get train availabilty for the current date

function checkFare(trainNo,avail) {
    fetch('https://api.railwayapi.com/v2/fare/train/'+ trainNo +'/source/'+ sourceStationId +'/dest/'+ destinationStationId +'/age/25/pref/'+ trainClass.val() +'/quota/GN/date/'+ date.val() +'/apikey/e9hyw3mvg3/').then( resp => {
        return resp.json();
    }).then( data => {
        console.log(data.fare);
        generateResult(data,avail);
    });
} // check fare for the train

function generateResult(data,avail) {
    loaderToggle(false); // disable the loader
    $('#table-body').empty();
    let status;
    if ( data.fare <= priceMax.val() ) {
        status = 'Alright';
    } else if ( data.fare > priceMax.val() ) {
        status = 'Pricey';
    } else {
        status = 'NA';
    }
    $('#table-body').append('<tr><td>'+ data.train.name +'</td><td>'+ data.train.number +'</td><td>'+ data.fare === null ? 'NA' : data.fare +'</td><td>'+ avail +'</td><td>'+ status +'</td></tr>')
    $('#result-card').css({
        'display': 'block'
    });
} // generate result based on the fare of the train and the budget of the user

function noResult() {
    loaderToggle(false); // disable the loader
    // show the result card
    $('#result-card').css({
        'display': 'block'
    });
    $('#result').empty();
    $('#result').append('<h3>No results found</h3>');
} // show if no trains found



button.on("click",  (event) => {
    event.preventDefault(); // prevent form post from reloading the page
    if ( validateForm() ) { // check if form is valid
        formToggle(true); // disable the form 
        console.log(date.val());
        $('#form-card').addClass('scale-transition'); // add animation to the to form
        $('#form-card').addClass('scale-out'); // add animation to the to form
        setTimeout( () => {
            $('#form-card').css({
                'display': 'none'
            });
        }, 500); // hide the input form
        loaderToggle(true); // show the loader
        getTrainsBetweenStations(); // get list of trains between stations
    }
}); // catch button press on back button

back.on('click', (event) => {
    event.preventDefault();
    // show the input form
    $('#form-card').css({
        'display': 'block',
        'hidden': 'false'
    });
    // hide the result form
    $('#result-card').css({
        'display': 'none'
    });
    formToggle(false); // enable the form
}); // catch button press on back button